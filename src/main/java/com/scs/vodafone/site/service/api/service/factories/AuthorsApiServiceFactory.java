package com.scs.vodafone.site.service.api.service.factories;

import com.scs.vodafone.site.service.api.service.AuthorsApiService;
import com.scs.vodafone.site.service.api.service.impl.AuthorsApiServiceImpl;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2020-08-20T12:39:25.398+02:00")
public class AuthorsApiServiceFactory {

    private final static AuthorsApiService service = new AuthorsApiServiceImpl();

    public static AuthorsApiService getAuthorsApi() {
        return service;
    }
}
