package com.scs.vodafone.site.service.api.service.mapper;

import com.scs.vodafone.site.service.api.model.Author;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface AuthorMapper {

    AuthorMapper INSTANCE = Mappers.getMapper(AuthorMapper.class);

    com.scs.vodafone.site.service.api.store.entity.Author mapToEntity(Author author);

    Author mapToModel(com.scs.vodafone.site.service.api.store.entity.Author author);

    List<Author> mapToModel(List<com.scs.vodafone.site.service.api.store.entity.Author> authorsList);
}
