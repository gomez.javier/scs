package com.scs.vodafone.site.service.api.store.repository.impl;

import com.scs.vodafone.site.service.api.store.entity.Author;
import com.scs.vodafone.site.service.api.store.entity.Book;
import com.scs.vodafone.site.service.api.store.repository.BookRepository;
import lombok.RequiredArgsConstructor;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;

@RequiredArgsConstructor
public class BookRepositoryImpl implements BookRepository {

    private static final String DASH_PREFIX = "-";
    private static final String SPECIAL_LIKE_CHAR = "%";
    private static final String ATTRIBUTE_AUTHOR = "author";
    private static final String ATTRIBUTE_TITLE = "title";
    private static final String ATTRIBUTE_NAME = "name";
    private static final String ATTRIBUTE_PUBLICATION_DATE = "publicationDate";
    private static final String ATTRIBUTE_ISBN = "isbn";
    private static final String ATTRIBUTE_UUID = "uuId";

    @Inject
    private EntityManager entityManager;

    @Resource
    private UserTransaction utx;

    @Override
    public List<Book> findBooks(Integer limit, Integer offset, List<String> sort, String title, String author, String publicationDate, String isbn) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Book> criteriaQuery = builder.createQuery(Book.class);
        Root<Book> bookRoot = criteriaQuery.from(Book.class);
        Join<Book, Author> join = bookRoot.join(ATTRIBUTE_AUTHOR);

        criteriaQuery.select(bookRoot);

        if (!isNull(title)) {
            criteriaQuery.where(builder.like(bookRoot.get(ATTRIBUTE_TITLE), SPECIAL_LIKE_CHAR + title + SPECIAL_LIKE_CHAR));
        }
        if (!isNull(author)) {
            criteriaQuery.where(builder.like(join.get(ATTRIBUTE_NAME), SPECIAL_LIKE_CHAR + author + SPECIAL_LIKE_CHAR));
        }
        if (!isNull(publicationDate)) {
            // TODO: validate date
            criteriaQuery.where(builder.like(bookRoot.get(ATTRIBUTE_PUBLICATION_DATE), SPECIAL_LIKE_CHAR + publicationDate + SPECIAL_LIKE_CHAR));
        }
        if (!isNull(isbn)) {
            criteriaQuery.where(builder.like(bookRoot.get(ATTRIBUTE_ISBN), SPECIAL_LIKE_CHAR + isbn + SPECIAL_LIKE_CHAR));
        }

        // sorting
        if (!isNull(sort) && !sort.isEmpty()) {
            List<Order> orderList = new ArrayList<>();
            for (String sortField : sort) {
                orderList.add(sortField.startsWith(DASH_PREFIX)
                        ? builder.desc(bookRoot.get(sortField.substring(1)))
                        : builder.asc(bookRoot.get(sortField)));
            }
            criteriaQuery.orderBy(orderList);
        }

        TypedQuery<Book> query = entityManager.createQuery(criteriaQuery);

        if (!isNull(offset)) {
            query.setFirstResult(offset);
        }
        if (!isNull(limit)) {
            query.setMaxResults(limit);
        }
        List<Book> resultList = query.getResultList();
        return (!isNull(resultList) && !resultList.isEmpty()) ? resultList : new ArrayList<>();
    }

    @Override
    public Book findByUuid(String uuid) {
        Query query = entityManager.createQuery("from Book a where a.uuId = :uuId")
                .setParameter(ATTRIBUTE_UUID, uuid);
        try {
            return (Book) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<Book> getAllBooks() {
        return null;
    }

    @Override
    public Book createBook(Book book) {
        try {
            utx.begin();
            entityManager.merge(book);
            utx.commit();
            return book;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Book updateBook(Book book) {
        try {
            utx.begin();
            Book updatedBook = entityManager.merge(book);
            utx.commit();
            return updatedBook;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Book deleteBook(String uuId) {
        try {
            Book bookToDelete = findByUuid(uuId);
            utx.begin();
            entityManager.remove(bookToDelete);
            utx.commit();
            return bookToDelete;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Book updateBookPartially(String uuId, Book book) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaUpdate<Book> criteriaUpdate = builder.createCriteriaUpdate(Book.class);
        Root<Book> bookRoot = criteriaUpdate.from(Book.class);

        if (!isNull(book.getUuId())) {
            criteriaUpdate.set(bookRoot.get(ATTRIBUTE_UUID), book.getUuId());
        }
        if (!isNull(book.getTitle())) {
            criteriaUpdate.set(bookRoot.get(ATTRIBUTE_TITLE), book.getTitle());
        }
        if (!isNull(book.getAuthor())) {
            criteriaUpdate.set(bookRoot.get(ATTRIBUTE_AUTHOR), book.getAuthor());
        }
        if (!isNull(book.getPublicationDate())) {
            criteriaUpdate.set(bookRoot.get(ATTRIBUTE_PUBLICATION_DATE), book.getPublicationDate());
        }
        if (!isNull(book.getIsbn())) {
            criteriaUpdate.set(bookRoot.get(ATTRIBUTE_ISBN), book.getIsbn());
        }

        criteriaUpdate.where(builder.equal(bookRoot.get(ATTRIBUTE_UUID), uuId));
        try {
            utx.begin();
            int rowUpdated = entityManager.createQuery(criteriaUpdate).executeUpdate();
            utx.commit();
            return rowUpdated != 0 ? book : null;
        } catch (Exception e) {
            return null;
        }
    }
}
