package com.scs.vodafone.site.service.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2020-08-20T12:39:25.398+02:00")
public class ServiceError {

    private String error = null;
    private String details = null;

    /**
     * A machine readable error string, which may be used to display additional information to the user. By convention, the string will consist of uppercase letter, with each word separated by underscores. (i.e. [A-Z_]*)
     **/

    @ApiModelProperty(example = "ERR_INVALID_STATUS", value = "A machine readable error string, which may be used to display additional information to the user. By convention, the string will consist of uppercase letter, with each word separated by underscores. (i.e. [A-Z_]*)")
    @JsonProperty("error")
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    /**
     * An (optional) text, specifying additional information about the error, such as a stacktrace. This information is not intended for end users. This key may be undefined on production versions of the service for performance reasons.
     **/
    @ApiModelProperty(example = "null", value = "An (optional) text, specifying additional information about the error, such as a stacktrace. This information is not intended for end users. This key may be undefined on production versions of the service for performance reasons.")
    @JsonProperty("details")
    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ServiceError serviceError = (ServiceError) o;
        return Objects.equals(error, serviceError.error) &&
                Objects.equals(details, serviceError.details);
    }

    @Override
    public int hashCode() {
        return Objects.hash(error, details);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ServiceError {\n");

        sb.append("    error: ").append(toIndentedString(error)).append("\n");
        sb.append("    details: ").append(toIndentedString(details)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

