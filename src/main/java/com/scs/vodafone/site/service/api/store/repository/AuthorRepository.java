package com.scs.vodafone.site.service.api.store.repository;

import com.scs.vodafone.site.service.api.store.entity.Author;
import com.scs.vodafone.site.service.api.store.entity.Book;

import java.util.List;

public interface AuthorRepository {

    List<Author> findAuthors(Integer limit, Integer offset, List<String> sort, String name);

    List<Book> getAuthorsBooks(String uuId, Integer limit, Integer offset, List<String> sort);

    Book getAuthorsBook(String authorUuId, String bookUuId);

    Author findByUuid(String uuId);

    Author createAuthor(Author author);

    Author updateAuthor(Author author);

    Author updateAuthorPartially(String uuId, Author author);

    Author deleteAuthor(String uuId);
}
