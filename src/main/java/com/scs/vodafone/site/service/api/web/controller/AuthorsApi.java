package com.scs.vodafone.site.service.api.web.controller;

import com.scs.vodafone.site.service.api.service.AuthorsApiService;
import com.scs.vodafone.site.service.api.web.exception.NotFoundException;
import com.scs.vodafone.site.service.api.model.Author;
import com.scs.vodafone.site.service.api.model.Book;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import io.swagger.jaxrs.PATCH;
import org.jboss.resteasy.annotations.cache.NoCache;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path("/authors")
@io.swagger.annotations.Api(description = "the authors API", tags = {"Authors API"})
@SwaggerDefinition(tags = {
        @Tag(name = "Authors API", description = "the authors API")
})
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2020-08-20T12:39:25.398+02:00")
public class AuthorsApi {

    @Inject
    private AuthorsApiService delegate;

    @PUT
    @Path("/{authorUuId}/books")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "Add author book", notes = "Adds the book to the list of this author's books", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 204, message = "The resource was successfully updated. No content is returned.", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Void.class)})
    public Response addAuthorBook(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("authorUuId") String authorUuId, @ApiParam(value = "", required = true) Book book, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.addAuthorBook(authorUuId, book, authorization, securityContext, uriInfo);
    }

    @POST
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "Create Author", notes = "Creates a new author", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 201, message = "The resource was successfully created", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Void.class)})
    public Response createAuthor(@ApiParam(value = "", required = true) Author author, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.createAuthor(author, authorization, securityContext, uriInfo);
    }

    @DELETE
    @Path("/{authorUuId}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "Delete Author", notes = "Removes the specified author", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 204, message = "The resource was successfully updated. No content is returned.", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Void.class)})
    public Response deleteAuthor(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("authorUuId") String authorUuId, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.deleteAuthor(authorUuId, authorization, securityContext, uriInfo);
    }

    @DELETE
    @Path("/{authorUuId}/books/{bookUuId}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "Delete Author", notes = "Removes the specified book from the authors list of books. Does not delete the book itself.", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 204, message = "The resource was successfully updated. No content is returned.", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Void.class)})
    public Response deleteAuthorBook(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("authorUuId") String authorUuId, @Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("bookUuId") String bookUuId, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.deleteAuthorBook(authorUuId, bookUuId, authorization, securityContext, uriInfo);
    }

    @GET
    @NoCache
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "List Authors", notes = "A filterable list of authors", response = Author.class, responseContainer = "List", tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "A list of authors that match the provided parameters. The length of the array will be <= limit. If no entities match the parameters, an empty array is returned.", response = Author.class, responseContainer = "List"),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Author.class, responseContainer = "List"),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Author.class, responseContainer = "List"),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Author.class, responseContainer = "List"),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Author.class, responseContainer = "List")})
    public Response findAuthors(@QueryParam("limit") Integer limit, @QueryParam("offset") Integer offset, @QueryParam("sort") List<String> sort, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @QueryParam("name") String name, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.findAuthors(limit, offset, sort, authorization, name, securityContext, uriInfo);
    }

    @GET
    @NoCache
    @Path("/{authorUuId}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "Get Author", notes = "Retrieves detailed information about the author", response = Author.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "The author details", response = Author.class),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Author.class),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Author.class),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Author.class),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Author.class)})
    public Response getAuthor(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("authorUuId") @NotNull String authorUuId, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.getAuthor(authorUuId, authorization, securityContext, uriInfo);
    }

    @GET
    @NoCache
    @Path("/{authorUuId}/books")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "Get Author's Books", notes = "Retrieves a list of the author's books", response = Book.class, responseContainer = "List", tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "A list of books that match the provided parameters. The length of the array will be <= limit. If no entities match the parameters, an empty array is returned.", response = Book.class, responseContainer = "List"),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Book.class, responseContainer = "List"),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Book.class, responseContainer = "List"),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Book.class, responseContainer = "List"),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Book.class, responseContainer = "List")})
    public Response getAuthorBooks(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("authorUuId") String authorUuId, @QueryParam("limit") Integer limit, @QueryParam("offset") Integer offset, @QueryParam("sort") List<String> sort, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.getAuthorBooks(authorUuId, limit, offset, sort, authorization, securityContext, uriInfo);
    }

    @OPTIONS
    @Path("/{authorUuId}/books/{bookUuId}")
    @io.swagger.annotations.ApiOperation(value = "", notes = "", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Response to the options request", response = Void.class)})
    public Response optionsAuthorBookDetail(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("bookUuId") String bookUuId, @Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("authorUuId") String authorUuId, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.optionsAuthorBookDetail(bookUuId, authorUuId, securityContext, uriInfo);
    }

    @OPTIONS
    @Path("/{authorUuId}/books")
    @io.swagger.annotations.ApiOperation(value = "", notes = "", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Response to the options request", response = Void.class)})
    public Response optionsAuthorBooks(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("authorUuId") String authorUuId, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.optionsAuthorBooks(authorUuId, securityContext, uriInfo);
    }

    @OPTIONS
    @Path("/{authorUuId}")
    @io.swagger.annotations.ApiOperation(value = "", notes = "", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Response to the options request", response = Void.class)})
    public Response optionsAuthorDetail(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("authorUuId") String authorUuId, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.optionsAuthorDetail(authorUuId, securityContext, uriInfo);
    }

    @OPTIONS
    @io.swagger.annotations.ApiOperation(value = "", notes = "", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Response to the options request", response = Void.class)})
    public Response optionsAuthors(@Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.optionsAuthors(securityContext, uriInfo);
    }

    @PUT
    @Path("/{authorUuId}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "Update Author", notes = "Updates the author with the provided data.", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 204, message = "The resource was successfully updated. No content is returned.", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Void.class)})
    public Response updateAuthor(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("authorUuId") String authorUuId, @ApiParam(value = "", required = true) Author author, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.updateAuthor(authorUuId, author, authorization, securityContext, uriInfo);
    }

    @PATCH
    @Path("/{authorUuId}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "Partially Update Author", notes = "Updates the author with only the provided attributes.", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 204, message = "The resource was successfully updated. No content is returned.", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Void.class)})
    public Response updateAuthorPartial(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("authorUuId") String authorUuId, @ApiParam(value = "", required = true) Author author, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.updateAuthorPartial(authorUuId, author, authorization, securityContext, uriInfo);
    }
}
