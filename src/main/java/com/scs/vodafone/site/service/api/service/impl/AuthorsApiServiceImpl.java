package com.scs.vodafone.site.service.api.service.impl;

import com.scs.vodafone.site.service.api.model.Author;
import com.scs.vodafone.site.service.api.model.Book;
import com.scs.vodafone.site.service.api.service.AuthorsApiService;
import com.scs.vodafone.site.service.api.service.mapper.AuthorMapper;
import com.scs.vodafone.site.service.api.service.mapper.BookMapper;
import com.scs.vodafone.site.service.api.store.repository.AuthorRepository;
import com.scs.vodafone.site.service.api.store.repository.BookRepository;
import com.scs.vodafone.site.service.api.web.ApiResponseMessage;
import com.scs.vodafone.site.service.api.web.exception.NotFoundException;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2020-08-20T12:39:25.398+02:00")
public class AuthorsApiServiceImpl extends AuthorsApiService {

    @Inject
    private AuthorRepository authorRepository;

    @Inject
    private BookRepository bookRepository;

    @Inject
    private AuthorMapper authorMapper = AuthorMapper.INSTANCE;

    @Inject
    private BookMapper bookMapper = BookMapper.INSTANCE;

    @Override
    public Response addAuthorBook(String authorUuId, Book book, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        Author author = authorMapper.mapToModel(authorRepository.findByUuid(authorUuId));
        Book bookToUpdate = null;

        if (!isNull(author)) {
            Book bookRetrieved = bookMapper.mapToModel(bookRepository.findByUuid(book.getUuId()));
            if (!isNull(bookRetrieved)) {
                // book already exists
                bookRetrieved.setAuthor(author);
                bookToUpdate = bookMapper.mapToModel(bookRepository.updateBook(bookMapper.mapToEntity(bookRetrieved)));
            } else {
                // book is new
                bookToUpdate = bookMapper.mapToModel(bookRepository.createBook(bookMapper.mapToEntity(book)));
            }
        } else {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not found Author with UUID = " + authorUuId + " to add Book"))
                    .build();
        }

        if (!isNull(bookToUpdate)) {
            return Response.noContent().build();
        } else {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Exception adding Book to Author with UUID = " + authorUuId))
                    .build();
        }
    }

    @Override
    public Response createAuthor(Author author, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        if (!isNull(authorRepository.createAuthor(authorMapper.mapToEntity(author)))) {
            return Response
                    .status(Response.Status.CREATED)
                    .build();
        } else {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Request was invalid or Author already exists in the DB"))
                    .build();
        }
    }

    @Override
    public Response deleteAuthor(String authorUuId, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        Author existingAuthor = authorMapper.mapToModel(authorRepository.findByUuid(authorUuId));
        if (!isNull(existingAuthor)) {
            // check for existing Books of this Author
            List<Book> authorsBooks = bookMapper.mapToModel(authorRepository.getAuthorsBooks(authorUuId, null, null, null));
            if (!isNull(authorsBooks) && !authorsBooks.isEmpty()) {
                for (Book book : authorsBooks) {
                    // Detach the Author of this Books
                    book.setAuthor(null);
                    bookRepository.updateBook(bookMapper.mapToEntity(book));
                }
            }
            if (!isNull(authorRepository.deleteAuthor(authorUuId))) {
                return Response.noContent().build();
            } else {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Exception deleting Author with UUID = " + authorUuId))
                        .build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Author to delete with UUID = " + authorUuId))
                    .build();
        }
    }

    @Override
    public Response deleteAuthorBook(String authorUuId, String bookUuId, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        Author existingAuthor = authorMapper.mapToModel(authorRepository.findByUuid(authorUuId));
        if (!isNull(existingAuthor)) {
            List<Book> authorsBooks = bookMapper.mapToModel(authorRepository.getAuthorsBooks(authorUuId, null, null, null));
            Optional<Book> optionalBook = authorsBooks.stream()
                    .filter(b -> b.getUuId().equals(bookUuId))
                    .findFirst();

            if (optionalBook.isPresent() && !isNull(optionalBook.get())) {
                Book book = optionalBook.get();
                book.setAuthor(null);
                bookRepository.updateBook(bookMapper.mapToEntity(book));
                return Response.noContent().build();
            } else {
                return Response
                        .status(Response.Status.NOT_FOUND)
                        .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Book to remove with UUID = " + bookUuId + " from Author with UUID = " + authorUuId))
                        .build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Author with UUID = " + authorUuId))
                    .build();
        }
    }

    @Override
    public Response findAuthors(Integer limit, Integer offset, List<String> sort, String authorization, String name, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        return Response.ok()
                .entity(authorMapper.mapToModel(
                        authorRepository.findAuthors(limit, offset, sort, name)))
                .build();
    }

    @Override
    public Response getAuthor(String authorUuId, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        Author author = authorMapper.mapToModel(authorRepository.findByUuid(authorUuId));
        if (!isNull(author)) {
            return Response.ok()
                    .entity(author)
                    .build();
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Author with UUID = " + authorUuId))
                    .build();
        }
    }

    @Override
    public Response getAuthorBooks(String authorUuId, Integer limit, Integer offset, List<String> sort, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        Author existingAuthor = authorMapper.mapToModel(authorRepository.findByUuid(authorUuId));
        if (!isNull(existingAuthor)) {
            return Response.ok()
                    .entity(bookMapper.mapToModel(
                            authorRepository.getAuthorsBooks(authorUuId, limit, offset, sort)))
                    .build();
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Author with UUID = " + authorUuId))
                    .build();
        }
    }

    @Override
    public Response optionsAuthorBookDetail(String bookUuId, String authorUuId, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {
        Author existingAuthor = authorMapper.mapToModel(authorRepository.findByUuid(authorUuId));
        if (!isNull(existingAuthor)) {
            Book book = bookMapper.mapToModel(authorRepository.getAuthorsBook(authorUuId, bookUuId));
            if (!isNull(book)) {
                return Response.ok()
                        .entity(book)
                        .build();
            } else {
                return Response.status(Response.Status.NOT_FOUND)
                        .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Book with UUID = " + bookUuId + " from Author with UUID = " + authorUuId))
                        .build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Author with UUID = " + authorUuId))
                    .build();
        }
    }

    @Override
    public Response optionsAuthorBooks(String authorUuId, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {
        Author existingAuthor = authorMapper.mapToModel(authorRepository.findByUuid(authorUuId));
        if (!isNull(existingAuthor)) {
            return Response.ok()
                    .entity(bookMapper.mapToModel(
                            authorRepository.getAuthorsBooks(authorUuId, null, null, null)))
                    .build();
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Author with UUID = " + authorUuId))
                    .build();
        }
    }

    @Override
    public Response optionsAuthorDetail(String authorUuId, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {
        Author author = authorMapper.mapToModel(authorRepository.findByUuid(authorUuId));
        if (!isNull(author)) {
            return Response.ok()
                    .entity(author)
                    .build();
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Author with UUID = " + authorUuId))
                    .build();
        }
    }

    @Override
    public Response optionsAuthors(SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {
        return Response.ok()
                .entity(authorMapper.mapToModel(
                        authorRepository.findAuthors(null, null, null, null)))
                .build();
    }

    @Override
    public Response updateAuthor(String authorUuId, Author author, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        Author existingAuthor = authorMapper.mapToModel(authorRepository.findByUuid(authorUuId));
        if (!isNull(existingAuthor)) {
            if (!isNull(authorRepository.updateAuthor(authorMapper.mapToEntity(author)))) {
                return Response.noContent().build();
            } else {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Exception updating Author with UUID = " + authorUuId))
                        .build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Author to update with UUID = " + authorUuId))
                    .build();
        }
    }

    @Override
    public Response updateAuthorPartial(String authorUuId, Author author, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        Author existingAuthor = authorMapper.mapToModel(authorRepository.findByUuid(authorUuId));
        if (!isNull(existingAuthor)) {
            if (!isNull(authorRepository.updateAuthorPartially(authorUuId, authorMapper.mapToEntity(author)))) {
                return Response.noContent().build();
            } else {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Exception partially updating Author with UUID = " + authorUuId))
                        .build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Author to partially update with UUID = " + authorUuId))
                    .build();
        }
    }
}
