package com.scs.vodafone.site.service.api.service.factories;

import com.scs.vodafone.site.service.api.service.HealthApiService;
import com.scs.vodafone.site.service.api.service.impl.HealthApiServiceImpl;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2020-08-20T12:39:25.398+02:00")
public class HealthApiServiceFactory {

    private final static HealthApiService service = new HealthApiServiceImpl();

    public static HealthApiService getHealthApi() {
        return service;
    }
}
