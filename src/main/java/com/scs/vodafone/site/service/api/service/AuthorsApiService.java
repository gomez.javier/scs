package com.scs.vodafone.site.service.api.service;

import com.scs.vodafone.site.service.api.web.exception.NotFoundException;
import com.scs.vodafone.site.service.api.model.Author;
import com.scs.vodafone.site.service.api.model.Book;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2020-08-20T12:39:25.398+02:00")
public abstract class AuthorsApiService {
    public abstract Response addAuthorBook(String authorUuId, Book book, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response createAuthor(Author author, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response deleteAuthor(String authorUuId, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response deleteAuthorBook(String authorUuId, String bookUuId, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response findAuthors(Integer limit, Integer offset, List<String> sort, String authorization, String name, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response getAuthor(String authorUuId, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response getAuthorBooks(String authorUuId, Integer limit, Integer offset, List<String> sort, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response optionsAuthorBookDetail(String bookUuId, String authorUuId, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response optionsAuthorBooks(String authorUuId, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response optionsAuthorDetail(String authorUuId, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response optionsAuthors(SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response updateAuthor(String authorUuId, Author author, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response updateAuthorPartial(String authorUuId, Author author, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;
}
