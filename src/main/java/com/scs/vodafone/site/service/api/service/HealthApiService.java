package com.scs.vodafone.site.service.api.service;

import com.scs.vodafone.site.service.api.web.exception.NotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2020-08-20T12:39:25.398+02:00")
public abstract class HealthApiService {
    public abstract Response checkHealth(SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response optionsHealth(SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;
}
