package com.scs.vodafone.site.service.api.service.impl;

import com.scs.vodafone.site.service.api.model.Author;
import com.scs.vodafone.site.service.api.model.Book;
import com.scs.vodafone.site.service.api.service.BooksApiService;
import com.scs.vodafone.site.service.api.service.mapper.AuthorMapper;
import com.scs.vodafone.site.service.api.service.mapper.BookMapper;
import com.scs.vodafone.site.service.api.store.repository.AuthorRepository;
import com.scs.vodafone.site.service.api.store.repository.BookRepository;
import com.scs.vodafone.site.service.api.web.ApiResponseMessage;
import com.scs.vodafone.site.service.api.web.exception.NotFoundException;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import java.util.List;

import static java.util.Objects.isNull;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2020-08-20T12:39:25.398+02:00")
public class BooksApiServiceImpl extends BooksApiService {

    @Inject
    private BookRepository bookRepository;

    @Inject
    private AuthorRepository authorRepository;

    @Inject
    private BookMapper bookMapper = BookMapper.INSTANCE;

    @Inject
    private AuthorMapper authorMapper = AuthorMapper.INSTANCE;

    @Override
    public Response createBook(Book book, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        Author existingAuthor = authorMapper.mapToModel(authorRepository.findByUuid(book.getAuthor().getUuId()));
        if (!isNull(existingAuthor)) {
            // check if book exists
            Book existingBook = bookMapper.mapToModel(bookRepository.findByUuid(book.getUuId()));
            if (isNull(existingBook)) {
                if (!isNull(bookRepository.createBook(bookMapper.mapToEntity(book)))) {
                    return Response
                            .status(Response.Status.CREATED)
                            .build();
                } else {
                    return Response
                            .status(Response.Status.INTERNAL_SERVER_ERROR)
                            .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Exception creating Book with UUID = " + book.getUuId())).build();
                }
            } else {
                return Response.status(Response.Status.CONFLICT)
                        .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Book with UUID = " + book.getUuId() + " already exists in the DB"))
                        .build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Author with UUID = " + book.getAuthor().getUuId()))
                    .build();
        }
    }

    @Override
    public Response deleteBook(String bookUuId, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        Book existingBook = bookMapper.mapToModel(bookRepository.findByUuid(bookUuId));
        if (!isNull(existingBook)) {
            if (!isNull(bookRepository.deleteBook(bookUuId))) {
                return Response
                        .noContent()
                        .build();
            } else {
                return Response
                        .status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Exception deleting Book with UUID = " + bookUuId)).build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Book to delete with UUID = " + bookUuId))
                    .build();
        }
    }

    @Override
    public Response findBooks(Integer limit, Integer offset, List<String> sort, String authorization, String title, String author, String publicationDate, String isbn, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        return Response.ok()
                .entity(bookMapper.mapToModel(
                        bookRepository.findBooks(limit, offset, sort, title, author, publicationDate, isbn)))
                .build();
    }

    @Override
    public Response getBook(String bookUuId, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        Book book = bookMapper.mapToModel(bookRepository.findByUuid(bookUuId));
        if (!isNull(book)) {
            return Response.ok()
                    .entity(book)
                    .build();
        } else {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Book with UUID = " + bookUuId))
                    .build();
        }
    }

    @Override
    public Response optionsBookDetail(String bookUuId, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {
        Book book = bookMapper.mapToModel(bookRepository.findByUuid(bookUuId));
        if (!isNull(book)) {
            return Response.ok()
                    .entity(book)
                    .build();
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Book with UUID = " + bookUuId))
                    .build();
        }
    }

    @Override
    public Response optionsBooks(SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {
        return Response.ok()
                .entity(bookMapper.mapToModel(
                        bookRepository.findBooks(null, null, null, null, null, null, null)))
                .build();
    }

    @Override
    public Response updateBook(String bookUuId, Book book, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        Book existingBook = bookMapper.mapToModel(bookRepository.findByUuid(bookUuId));
        if (!isNull(existingBook)) {
            if (!isNull(bookRepository.updateBook(bookMapper.mapToEntity(book)))) {
                return Response.noContent().build();
            } else {
                return Response
                        .status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Exception updating Book with UUID = " + bookUuId)).build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Book to update with UUID = " + bookUuId))
                    .build();
        }
    }

    @Override
    public Response updateBookPartial(String bookUuId, Book book, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {

        if (isNull(securityContext.getUserPrincipal())) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();
        }

        Book existingBook = bookMapper.mapToModel(bookRepository.findByUuid(bookUuId));
        if (!isNull(existingBook)) {
            if (!isNull(bookRepository.updateBookPartially(bookUuId, bookMapper.mapToEntity(book)))) {
                return Response.noContent().build();
            } else {
                return Response
                        .status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Exception partially updating Book with UUID = " + bookUuId)).build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Not Found Book to partially update with UUID = " + bookUuId))
                    .build();
        }
    }
}
