package com.scs.vodafone.site.service.api.store.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AUTHOR")
@RequiredArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public class Author {

    @Id
    @Column(name = "UUID")
    String uuId;

    @Column(name = "NAME")
    String name;
}
