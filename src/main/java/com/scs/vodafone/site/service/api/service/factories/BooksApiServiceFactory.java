package com.scs.vodafone.site.service.api.service.factories;

import com.scs.vodafone.site.service.api.service.BooksApiService;
import com.scs.vodafone.site.service.api.service.impl.BooksApiServiceImpl;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2020-08-20T12:39:25.398+02:00")
public class BooksApiServiceFactory {

    private final static BooksApiService service = new BooksApiServiceImpl();

    public static BooksApiService getBooksApi() {
        return service;
    }
}
