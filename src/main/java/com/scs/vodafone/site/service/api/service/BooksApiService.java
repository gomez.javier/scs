package com.scs.vodafone.site.service.api.service;

import com.scs.vodafone.site.service.api.web.exception.NotFoundException;
import com.scs.vodafone.site.service.api.model.Book;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2020-08-20T12:39:25.398+02:00")
public abstract class BooksApiService {
    public abstract Response createBook(Book book, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response deleteBook(String bookUuId, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response findBooks(Integer limit, Integer offset, List<String> sort, String authorization, String title, String author, String publicationDate, String isbn, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response getBook(String bookUuId, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response optionsBookDetail(String bookUuId, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response optionsBooks(SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response updateBook(String bookUuId, Book book, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;

    public abstract Response updateBookPartial(String bookUuId, Book book, String authorization, SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException;
}
