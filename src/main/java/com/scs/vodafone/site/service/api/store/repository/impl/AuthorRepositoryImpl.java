package com.scs.vodafone.site.service.api.store.repository.impl;

import com.scs.vodafone.site.service.api.store.entity.Author;
import com.scs.vodafone.site.service.api.store.entity.Book;
import com.scs.vodafone.site.service.api.store.repository.AuthorRepository;
import lombok.RequiredArgsConstructor;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;

@RequiredArgsConstructor
public class AuthorRepositoryImpl implements AuthorRepository {

    private static final String SPECIAL_LIKE_CHAR = "%";
    private static final String DASH_PREFIX = "-";
    private static final String ATTRIBUTE_NAME = "name";
    private static final String ATTRIBUTE_UUID = "uuId";

    @Inject
    private EntityManager entityManager;

    @Resource
    private UserTransaction utx;

    @Override
    public List<Author> findAuthors(Integer limit, Integer offset, List<String> sort, String name) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Author> criteriaQuery = builder.createQuery(Author.class);
        Root<Author> authorRoot = criteriaQuery.from(Author.class);
        criteriaQuery.select(authorRoot);

        // filter by name
        if (!isNull(name)) {
            criteriaQuery.where(builder.like(authorRoot.get(ATTRIBUTE_NAME), SPECIAL_LIKE_CHAR + name + SPECIAL_LIKE_CHAR));
        }

        // sorting
        if (!isNull(sort) && !sort.isEmpty()) {
            List<Order> orderList = new ArrayList<>();
            for (String sortField : sort) {
                // TODO: pending validation of fields
                orderList.add(sortField.startsWith(DASH_PREFIX)
                        ? builder.desc(authorRoot.get(sortField.substring(1)))
                        : builder.asc(authorRoot.get(sortField)));
            }
            criteriaQuery.orderBy(orderList);
        }

        TypedQuery<Author> query = entityManager.createQuery(criteriaQuery);

        // offset and limit
        if (!isNull(offset)) {
            query.setFirstResult(offset);
        }
        if (!isNull(limit)) {
            query.setMaxResults(limit);
        }

        return query.getResultList();
    }

    @Override
    public List<Book> getAuthorsBooks(String uuId, Integer limit, Integer offset, List<String> sort) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Book> criteriaQuery = builder.createQuery(Book.class);
        Root<Book> bookRoot = criteriaQuery.from(Book.class);
        Join<Book, Author> join = bookRoot.join("author");
        criteriaQuery.select(bookRoot).where(builder.equal(join.get("uuId"), uuId));

        // sorting
        if (!isNull(sort) && !sort.isEmpty()) {
            List<Order> orderList = new ArrayList<>();
            for (String sortField : sort) {
                orderList.add(sortField.startsWith(DASH_PREFIX)
                        ? builder.desc(bookRoot.get(sortField.substring(1)))
                        : builder.asc(bookRoot.get(sortField)));
            }
            criteriaQuery.orderBy(orderList);
        }

        TypedQuery<Book> query = entityManager.createQuery(criteriaQuery);

        if (!isNull(offset)) {
            query.setFirstResult(offset);
        }
        if (!isNull(limit)) {
            query.setMaxResults(limit);
        }
        return query.getResultList();
    }

    @Override
    public Book getAuthorsBook(String authorUuId, String bookUuId) {
        Query query = entityManager.createQuery("from Book b where b.uuId = :bookUuId and b.author.uuId = :authorUuId")
                .setParameter("bookUuId", bookUuId)
                .setParameter("authorUuId", authorUuId);
        try {
            return (Book) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Author findByUuid(String uuId) {
        Query query = entityManager.createQuery("from Author a where a.uuId = :uuId")
                .setParameter(ATTRIBUTE_UUID, uuId);
        try {
            return (Author) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Author createAuthor(Author author) {
        try {
            if (isNull(findByUuid(author.getUuId()))) {
                utx.begin();
                entityManager.persist(author);
                utx.commit();
                return author;
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Author updateAuthorPartially(String uuId, Author author) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaUpdate<Author> criteriaUpdate = builder.createCriteriaUpdate(Author.class);
        Root<Author> authorRoot = criteriaUpdate.from(Author.class);

        if (!isNull(author.getUuId())) {
            criteriaUpdate.set(authorRoot.get(ATTRIBUTE_UUID), author.getUuId());
        }
        if (!isNull(author.getName())) {
            criteriaUpdate.set(authorRoot.get(ATTRIBUTE_NAME), author.getName());
        }

        criteriaUpdate.where(builder.equal(authorRoot.get(ATTRIBUTE_UUID), uuId));
        try {
            utx.begin();
            int rowUpdated = entityManager.createQuery(criteriaUpdate).executeUpdate();
            utx.commit();
            return rowUpdated != 0 ? author : null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Author updateAuthor(Author author) {
        try {
            utx.begin();
            Author updatedAuthor = entityManager.merge(author);
            utx.commit();
            return updatedAuthor;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Author deleteAuthor(String uuId) {
        try {
            Author authorToDelete = findByUuid(uuId);
            utx.begin();
            entityManager.remove(authorToDelete);
            utx.commit();
            return authorToDelete;
        } catch (Exception e) {
            return null;
        }
    }
}
