package com.scs.vodafone.site.service.api.service.impl;

import com.scs.vodafone.site.service.api.service.HealthApiService;
import com.scs.vodafone.site.service.api.web.exception.NotFoundException;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import static java.util.Objects.isNull;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2020-08-20T12:39:25.398+02:00")
public class HealthApiServiceImpl extends HealthApiService {

    private static final String HEALTHCHECK_QUERY = "select 1 from dual";

    @Inject
    private EntityManager entityManager;

    @Override
    public Response checkHealth(SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {
        return checkDatabaseConnection();
    }

    @Override
    public Response optionsHealth(SecurityContext securityContext, UriInfo uriInfo)
            throws NotFoundException {
        return checkDatabaseConnection();
    }

    private Response checkDatabaseConnection() {
        if (!isNull(entityManager.createNativeQuery(HEALTHCHECK_QUERY).getSingleResult())) {
            return Response.noContent().build();
        } else {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();
        }
    }
}
