package com.scs.vodafone.site.service.api.service.mapper;

import com.scs.vodafone.site.service.api.model.Book;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface BookMapper {

    BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

    @Mapping(target = "publicationDate", source = "publicationDate",
            dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    com.scs.vodafone.site.service.api.store.entity.Book mapToEntity(Book book);

    @Mapping(target = "publicationDate", source = "publicationDate",
            dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    Book mapToModel(com.scs.vodafone.site.service.api.store.entity.Book book);

    List<Book> mapToModel(List<com.scs.vodafone.site.service.api.store.entity.Book> booksList);
}
