package com.scs.vodafone.site.service.api.store.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "BOOK")
@RequiredArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public class Book {

    @Id
    @Column(name = "UUID")
    String uuId;

    @Column(name = "TITLE")
    String title;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "AUTHOR", referencedColumnName = "UUID")
    Author author;

    @Column(name = "PUBLICATION_DATE")
    Date publicationDate;

    @Column(name = "ISBN")
    String isbn;

}
