package com.scs.vodafone.site.service.api.web.configuration;

import io.swagger.jaxrs.config.BeanConfig;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class RestApplication extends Application {

    public RestApplication() {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("v1");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:8080");
        beanConfig.setBasePath("/example-service/example/v1/api");
        beanConfig.setResourcePackage("com.scs.vodafone.site.service.api.web.controller");
        beanConfig.setTitle("site-service Sample");
        beanConfig.setDescription("API Documentation for site-service sample");
        beanConfig.setScan(true);
    }
}