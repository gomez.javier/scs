package com.scs.vodafone.site.service.api.web.controller;

import com.scs.vodafone.site.service.api.service.HealthApiService;
import com.scs.vodafone.site.service.api.web.exception.NotFoundException;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import org.jboss.resteasy.annotations.cache.NoCache;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

@Path("/health")
@io.swagger.annotations.Api(description = "the health API", tags = {"Health API"})
@SwaggerDefinition(tags = {
        @Tag(name = "Health API", description = "the health API")
})
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2020-08-20T12:39:25.398+02:00")
public class HealthApi {

    @Inject
    private HealthApiService delegate;

    @GET
    @NoCache
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "Health Check", notes = "Performs a health check on the service. Will only return a successful status code if database connectivity can be verified.", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 204, message = "The service is up and running and a database connection could be established.", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 503, message = "The service is up and running but no database connection could be established.", response = Void.class)})
    public Response checkHealth(@Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.checkHealth(securityContext, uriInfo);
    }

    @OPTIONS
    @io.swagger.annotations.ApiOperation(value = "", notes = "", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Response to the options request", response = Void.class)})
    public Response optionsHealth(@Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.optionsHealth(securityContext, uriInfo);
    }
}
