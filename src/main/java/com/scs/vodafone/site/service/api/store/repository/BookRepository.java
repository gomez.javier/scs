package com.scs.vodafone.site.service.api.store.repository;

import com.scs.vodafone.site.service.api.store.entity.Book;

import java.util.List;

public interface BookRepository {

    List<Book> findBooks(Integer limit, Integer offset, List<String> sort, String title, String author, String publicationDate, String isbn);

    Book findByUuid(String uuId);

    List<Book> getAllBooks();

    Book createBook(Book Book);

    Book updateBook(Book Book);

    Book updateBookPartially(String uuId, Book book);

    Book deleteBook(String uuId);
}
