package com.scs.vodafone.site.service.api.web.controller;

import com.scs.vodafone.site.service.api.model.Book;
import com.scs.vodafone.site.service.api.service.BooksApiService;
import com.scs.vodafone.site.service.api.web.exception.NotFoundException;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import io.swagger.jaxrs.PATCH;
import org.jboss.resteasy.annotations.cache.NoCache;

import javax.inject.Inject;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path("/books")
@io.swagger.annotations.Api(description = "the books API", tags = {"Books API"})
@SwaggerDefinition(tags = {
        @Tag(name = "Books API", description = "the books API")
})
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2020-08-20T12:39:25.398+02:00")
public class BooksApi {

    @Inject
    private BooksApiService delegate;

    @POST
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "Create Book", notes = "Creates a new Book. The specified author must already exist in the database.", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 201, message = "The resource was successfully created", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Void.class)})
    public Response createBook(@ApiParam(value = "", required = true) Book book, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.createBook(book, authorization, securityContext, uriInfo);
    }

    @DELETE
    @Path("/{bookUuId}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "Delete Book", notes = "Deletes the specified book", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 204, message = "The resource was successfully updated. No content is returned.", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Void.class)})
    public Response deleteBook(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("bookUuId") String bookUuId, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.deleteBook(bookUuId, authorization, securityContext, uriInfo);
    }

    @GET
    @NoCache
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "List Books", notes = "A filterable list of books", response = Book.class, responseContainer = "List", tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "A list of books that match the provided parameters. The length of the array will be <= limit. If no entities match the parameters, an empty array is returned.", response = Book.class, responseContainer = "List"),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Book.class, responseContainer = "List"),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Book.class, responseContainer = "List"),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Book.class, responseContainer = "List"),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Book.class, responseContainer = "List")})
    public Response findBooks(@QueryParam("limit") Integer limit, @QueryParam("offset") Integer offset, @QueryParam("sort") List<String> sort, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @QueryParam("title") String title, @QueryParam("author") String author, @QueryParam("publicationDate") String publicationDate, @QueryParam("isbn") String isbn, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.findBooks(limit, offset, sort, authorization, title, author, publicationDate, isbn, securityContext, uriInfo);
    }

    @GET
    @NoCache
    @Path("/{bookUuId}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "Get Book", notes = "Retrieves detailed information about the book", response = Book.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "The book details", response = Book.class),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Book.class),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Book.class),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Book.class),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Book.class)})
    public Response getBook(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("bookUuId") String bookUuId, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.getBook(bookUuId, authorization, securityContext, uriInfo);
    }

    @OPTIONS
    @Path("/{bookUuId}")
    @io.swagger.annotations.ApiOperation(value = "", notes = "", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Response to the options request", response = Void.class)})
    public Response optionsBookDetail(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("bookUuId") String bookUuId, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.optionsBookDetail(bookUuId, securityContext, uriInfo);
    }

    @OPTIONS
    @io.swagger.annotations.ApiOperation(value = "", notes = "", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Response to the options request", response = Void.class)})
    public Response optionsBooks(@Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.optionsBooks(securityContext, uriInfo);
    }

    @PUT
    @Path("/{bookUuId}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "Update Book", notes = "Updates the specified book with the provided data.", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 204, message = "The resource was successfully updated. No content is returned.", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Void.class)})
    public Response updateBook(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("bookUuId") String bookUuId, @ApiParam(value = "", required = true) Book book, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.updateBook(bookUuId, book, authorization, securityContext, uriInfo);
    }

    @PATCH
    @Path("/{bookUuId}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "Partially Update Book", notes = "Updates the specified book with only the provided attributes.", response = Void.class, tags = {})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 204, message = "The resource was successfully updated. No content is returned.", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 400, message = "Request was invalid", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 401, message = "Auth-Token header is missing or not authorized for this request", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 404, message = "The resource does not exist", response = Void.class),

            @io.swagger.annotations.ApiResponse(code = 409, message = "There is a problem with the resource. This may be caused by an association with another entity or the status of the parent resource.", response = Void.class)})
    public Response updateBookPartial(@Pattern(regexp = "[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}") @Size(min = 36) @PathParam("bookUuId") String bookUuId, @ApiParam(value = "", required = true) Book book, @ApiParam(value = "The Keycloak bearer token. Technically not required but calls without this parameter will result in a 401 response.", defaultValue = "Bearer {{token}}") @HeaderParam("Authorization") String authorization, @Context SecurityContext securityContext, @Context UriInfo uriInfo)
            throws NotFoundException {
        return delegate.updateBookPartial(bookUuId, book, authorization, securityContext, uriInfo);
    }
}
