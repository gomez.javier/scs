DROP TABLE BOOK;
DROP TABLE AUTHOR;
CREATE TABLE AUTHOR
(
    UUID VARCHAR2(36 CHAR)  NOT NULL,
    NAME VARCHAR2(255 CHAR) NOT NULL,
    CONSTRAINT AUTHOR_PK PRIMARY KEY (UUID) ENABLE
);

CREATE TABLE BOOK
(
    UUID             VARCHAR2(36 CHAR)  NOT NULL,
    TITLE            VARCHAR2(255 CHAR) NOT NULL,
    AUTHOR           VARCHAR2(36 CHAR),
    PUBLICATION_DATE DATE               NOT NULL,
    ISBN             VARCHAR2(13 CHAR)  NOT NULL,
    CONSTRAINT BOOK_PK PRIMARY KEY (UUID) ENABLE,
    CONSTRAINT AUTHOR_FK FOREIGN KEY (AUTHOR) REFERENCES AUTHOR (UUID) ENABLE
);
