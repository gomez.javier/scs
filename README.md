# README

Since I couldn't contact you via chat yet, I created this README about the task.

The pom file should help you get started but will also contain a bunch of configurations which won't work for you yet.


## Database Access

jdbc:oracle:thin:@vosimaoracle.chrz68ehvvqu.eu-central-1.rds.amazonaws.com:1521:ORCL
user: 
pw: 

## Keycloak Server

https://dev.vosima.scs-luettgen.com:8444/auth
user: test
pw: test

User credentials will let you authenticate for the Realm Vosima, but not if navigate to above url with your browser

The file `Keycloak.postman_collection.json` contains a Postman request with which you can obtain your auth token from Keycloak

